const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update',(req,res)=>{
    if(req.body.id && req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products[req.body.id]=product;
        res.status(202).send(product);
    }else{
        res.status(500).send('There are no products with this id')
    }
});


app.delete('/delete' , (req, res) => {
    if(req.body.productName) {
        var isOk = 0;
        for( let i=0; i < products.length ; i++)
        {       if(products[i].productName === req.body.productName){
                isOk = 1;
                products.splice(i,1);
                res.status(200).send("The product has been deleted");
                break;
            }
            
        }
        if(!isOk)
            {
                res.status(500).send("There is no product with this name")
            }    
    }
    else {
    res.status(500).send("No product with this name!")
    }
})

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});
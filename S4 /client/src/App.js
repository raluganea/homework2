import React, { Component } from 'react';
import './App.css';

import {AddProduct} from './AddProduct'
import{ProductList} from './ProductList'

class App extends Component {
  constructor(props){
    super(props);
    this.state={};
    this.state.products=[]
  }

  onProductAdded =(product)=>{
    this.state.products.push(product);
    let products=this.state.products;
    this.setState({
      products:products
  })
}

  render() {
    return (
     <React.Fragment>
     <div>
      <AddProduct handleAdd={this.onProductAdded}/>
      <ProductList title="Products" source={this.state.products}/>
      </div>
      </React.Fragment>
    );
  }
}




export default App;
